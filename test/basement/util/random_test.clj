(ns basement.util.random-test
  (:require [clojure.test :refer :all]
            [basement.util.random :refer :all]))

(deftest test-random

  (testing "random pin has size 4"
    (let [pin (get-random-pin)]
      (is (= (.length pin) 4))))
)
