// this is a script to generate test-users in the mongodb database

db.users.insert({
    "nickname": "bebi",
    "firstname": "Bernhardt",
    "lastname": "Bieber",
    "email": "bebi@example.org",
    "contribution-receipt": false,
    "pin": 1234});

db.users.insert({
    "nickname": "foobar",
    "firstname": "Foo",
    "lastname": "Bar",
    "email": "foobar@example.org",
    "contribution-receipt": true});

db.users.insert({
    "nickname": "mulder",
    "firstname": "Fox",
    "lastname": "Mulder",
    "email": "mulder@example.org"});

db.users.insert({
    "nickname": "scully",
    "email": "scully@example.org"});

db.users.insert({
    "nickname": "elbert",
    "firstname": "Elbert",
    "lastname": "Erli",
    "contribution-receipt": false,
    "pin": 1111});

db.users.insert({
    "nickname": "bertel",
    "firstname": "Dagobert",
    "lastname": "Duck",
    "email": "d.duck@example.org",
    "contribution-receipt": true,
    "pin": 2323});
