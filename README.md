# rzl-basement
RZL Basement is a replacement for the traditional benutzerdb backend. It uses clojure, mongodb as storage backend and is interfaced via a HATEOS RESTful webservice.

## Status
The current Build Status is: [![Build Status](https://travis-ci.org/raumzeitlabor/rzl-basement.svg?branch=master)](https://travis-ci.org/raumzeitlabor/rzl-basement)

Bug reports and feature requests go here: [https://github.com/raumzeitlabor/rzl-basement/issues](https://github.com/raumzeitlabor/rzl-basement/issues)

## Prerequisites

You will need [Leiningen][] 2.0.0 or above installed.

[leiningen]: https://github.com/technomancy/leiningen

## Running

To start a web server for the application, run:

    lein ring server

MongoDB should be running on localhost:

## Developing

You can start your ring webserver inside your IDEs REPL with:
```clojure
(require 'basement.handler)
(use 'ring.adapter.jetty)
(defonce server (run-jetty #'basement.handler/basement {:port 8080 :join? false}))
```

## License
ISC, see LICENSE
