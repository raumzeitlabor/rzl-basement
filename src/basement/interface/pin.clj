(ns basement.interface.pin
  (:require [basement.api.users :as users]
            [basement.model.pin-decorator :as decorator]))

(defn get-pin [id]
  (let [pin (users/get-pin id)]
    (decorator/decorate-pin pin id)))

(defn delete-pin [id]
  (users/delete-pin id)
  "")

(defn reset-pin [id]
  (users/reset-pin id)
  (get-pin id))

(defn filter-pins [pinlist]
  (map (fn [pins] (:pin pins)) pinlist)
  )

(defn list-pins []
 {:valid-pins (filter-pins (users/all-pins))})
