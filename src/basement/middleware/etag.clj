(ns basement.middleware.etag
  (:require [ring.util.response :refer :all]))

(defn calc-etag [{body :body}]
  (str (hash body)))

(defn add-etag [res etag]
  (header res "ETag" etag))

(defn not-modified [] (status (response "") 304))

(defn etag
  "Middleware that uses the java hash function to add a ETag header to the body"
  [handler]
  (fn [request]
    (let [res (handler request)
          etag (calc-etag res)
          tagged (add-etag res etag)
          req-etag (get-in request [:headers "if-none-match"])]
      (if (= etag req-etag)
       (not-modified)
        tagged))))
