(ns basement.server
  (:gen-class)
  (:require [basement.handler :refer :all]
            [ring.adapter.jetty :refer :all]))

(defn -main []
	(run-jetty basement {:port 8080}))
