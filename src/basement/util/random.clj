(ns basement.util.random)

(defn get-random-pin []
  (apply str (repeatedly 4 #(rand-int 9))))
