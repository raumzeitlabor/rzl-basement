(ns basement.model.pin-decorator
  (:require [basement.util.json :as json]))

(defn uri-self [id]
  (str "/pin/" id))

(defn uri-delete [id]
  (str "/pin/" id))

(defn uri-reset [id]
  (str "/pin/" id))

(defn add-delete [id]
  (json/link "delete-pin"
             "application/org.raumzeitlabor.benutzerdb-v1+json"
             (uri-delete id)
             "DELETE"))

(defn add-reset [id]
  (json/link "reset-pin"
             "application/org.raumzeitlabor.benutzerdb-v1+json"
             (uri-reset id)
             "POST"))

(defn add-self [id]
  (json/link "self"
            "application/org.raumzeitlabor.benutzerdb-v1+json"
             (uri-self id)))

(defn decorate-pin [pin id]
  (if (nil? (:pin pin))
    nil
    (assoc pin
      :link [(add-delete id)
             (add-reset id)
             (add-self id)])))
